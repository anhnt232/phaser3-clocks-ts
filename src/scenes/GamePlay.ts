import 'phaser';
import CONFIG from "../../config";

const WATING = 0;
const PLAY = 1;

var gameOptions = {

    // grid size, in pixels
    gridSize: 55,

    // level width, in tiles
    levelWidth: 8,

    // level height, in tiles
    levelHeight: 8,

    // ball speed, in pixels per second
    ballSpeed: 600,

    // starting level
    startingLevel: 0,

    offsetX: -50,
    offsetY: 150,

    score: 0,
    localStorageName: "clockScore"
}

export default class GamePlay extends Phaser.Scene {
    canFire : boolean;
    clocksReached : number;
    totalClocks : number;
    clocksArray : any;
    handGroup : any;
    clockGroup: any;
    activeClock : any;
    ball : any;
    gameState: number;
    score: number;
    bestScore: number;
    logo: Phaser.GameObjects.Sprite;
    scoreText: Phaser.GameObjects.BitmapText;
    bestScoreText: Phaser.GameObjects.BitmapText;
    playButton: Phaser.GameObjects.Sprite;


    constructor() {
        super("GamePlay");
		this.gameState = WATING;
    }
    preload() {
        this.load.image("smallclockface", "assets/sprites/smallclockface.png");
        this.load.image("bigclockface", "assets/sprites/bigclockface.png");
        this.load.image("ball", "assets/sprites/ball.png");
        this.load.spritesheet("smallclock", "assets/sprites/smallclock.png", {
            frameWidth: 70,
            frameHeight: 70
        });
        this.load.spritesheet("smallhand", "assets/sprites/smallhand.png", {
            frameWidth: 70,
            frameHeight: 70
        });
        this.load.spritesheet("bigclock", "assets/sprites/bigclock.png", {
            frameWidth: 140,
            frameHeight: 140
        });
        this.load.spritesheet("bighand", "assets/sprites/bighand.png", {
            frameWidth: 140,
            frameHeight: 140
        });

		this.load.image("gameOver", "assets/sprites/GameOver.png");
		this.load.image("logo", "assets/sprites/Banner_hifpt.png");
        this.load.image("play", "assets/sprites/play.png");
        this.load.image("scorepanel", "assets/sprites/scorepanel.png");
        this.load.image("scorelabels", "assets/sprites/scorelabels.png");
        this.load.bitmapFont("font", "assets/fonts/font.png", "assets/fonts/font.fnt");
    }
    create() {
        this.score = gameOptions.score;
        this.bestScore = localStorage.getItem(gameOptions.localStorageName) == null ? 0 : Number(localStorage.getItem(gameOptions.localStorageName));

        this.logo = this.add.sprite(this.scale.width / 2, this.scale.height -10, "logo");
        this.logo.setOrigin(0.5, 1);
        this.logo.setScale(1);

        let x = 150
        let y = 10
        var scorelabels = this.add.sprite(x, y, "scorelabels");
        scorelabels.setOrigin(0,0);
        scorelabels.setScale(0.8);
        var scorepanel = this.add.sprite(x, y+30, "scorepanel");
        scorepanel.setOrigin(0,0);
        scorepanel.setScale(0.8);
        this.scoreText = this.add.bitmapText( x+15, y+50, "font", this.score.toString(), 50);
        this.scoreText.setOrigin(0, 0);
        this.bestScoreText = this.add.bitmapText( x+290, y+50, "font", this.bestScore.toString(), 50);
        this.bestScoreText.setOrigin(0, 0);

        if(this.gameState == PLAY){
            // player can fire now
            this.canFire = true;

            // clocks reached so far, just one, the one we start from
            this.clocksReached = 1;

            // total clocks in the level, about to be loaded
            this.totalClocks = 0;

            // array containing all clocks
            this.clocksArray = [];

            // physics group which contains all clock hands
            this.handGroup = this.physics.add.group();

            // physics group which contains all clocks
            this.clockGroup = this.physics.add.group();

            // loop through all current level items
            for(let i = 0; i < levels[gameOptions.startingLevel].tiledOutput.length; i ++) {

                // switching among possible values
                switch(levels[gameOptions.startingLevel].tiledOutput[i]) {

                    // small clock
                    case 1:
                        this.clocksArray.push(this.placeClock(new Phaser.Math.Vector2(i % gameOptions.levelWidth * 2 + 1, Math.floor(i / gameOptions.levelHeight) * 2 + 1), "small"));
                        break;

                    // big clock
                    case 2:
                        this.clocksArray.push(this.placeClock(new Phaser.Math.Vector2(i % gameOptions.levelWidth * 2 + 2, Math.floor(i / gameOptions.levelHeight) * 2), "big"));
                        break;
                }
            }

            // pick a random clock and make it the active clock
            this.activeClock = Phaser.Utils.Array.GetRandom(this.clocksArray);

            // change active clock appearance
            this.activeClock.setFrame(1);
            this.activeClock.tint = 0x2babca;
            this.activeClock.face.visible = true;
            this.activeClock.hand.setFrame(1);
            this.activeClock.hand.tint = 0xffffff;

            // add the ball
            this.ball = this.physics.add.sprite(this.scale.width / 2, this.scale.height / 2, "ball");

            // the ball is not visible at the beginning of the game
            this.ball.visible = false;

            // set ball to collide with world bounds
            this.ball.body.collideWorldBounds = true;

            // set ball to listen to world bounds collision
            this.ball.body.onWorldBounds = true;

            // when something (the ball) collide with world bounds...
            this.physics.world.on("worldbounds", function() {

                // restart the game
                this.gameOver();

            }, this);

            // wait for player input then call "throwBall" method
            this.input.on("pointerdown", this.throwBall, this);

            // handle overlap between the ball and "clockGroup" group, then call "handleOverlap" method
            this.physics.add.overlap(this.ball, this.clockGroup, this.handleOverlap, null, this);
        }
        else{
            var text = this.add.text(this.scale.width/2 , this.scale.height/4, "Welcome to", {
				font: "bold 70px Arial",
                color: "#6e43d9"
			});
			text.setOrigin(0.5, 1);
            var text = this.add.text(this.scale.width/2 , this.scale.height/4+100, "Clocks Game", {
				font: "bold 70px Arial",
                color: "#6e43d9"
			});
			text.setOrigin(0.5, 1);
            this.playButton = this.add.sprite(this.scale.width/2, this.scale.height/2, "play");
			this.playButton.setScale(1)
			this.playButton.setOrigin(0.5,0.5);
			this.playButton.setInteractive();
			this.playButton.on("pointerdown", function(){
				this.gameState = PLAY;
				this.scene.start("GamePlay");
			}, this)
        }
    }

    updateScore(){
        this.score += 10;
        if(this.score > this.bestScore){
            this.bestScore = this.score;
            this.bestScoreText.text = this.bestScore.toString();
        }
        this.scoreText.text = this.score.toString();
    }
    gameOver(){
        // shake the camera
        this.cameras.main.shake(500, 0.01);

        this.gameState = WATING;
        gameOptions.score = 0;
        
        localStorage.setItem(gameOptions.localStorageName, this.bestScore.toString());
        var gameOver = this.add.sprite(this.scale.width/2, this.scale.height/2, "gameOver");
        gameOver.setScale(1)
        gameOver.setOrigin(0.5,0.5);
        // restart the game in two seconds
        this.time.addEvent({
            delay: 4000,
            callbackScope: this,
            callback: function(){
                this.scene.start("GamePlay")
            }
        });
    }

    // method to place a clock on the stage, given the coordinates and a "small" or "big" prefix
    placeClock(clockCoordinates, prefix) {

        // clock creation
        let clockSprite = this.clockGroup.create(gameOptions.offsetX + clockCoordinates.x * gameOptions.gridSize, gameOptions.offsetY + clockCoordinates.y * gameOptions.gridSize, prefix + "clock");
        clockSprite.setScale(1.5);

        // faceSprite is the clock face
        let faceSprite = this.add.sprite(clockSprite.x, clockSprite.y, prefix + "clockface");
        faceSprite.setScale(1.5);

        // clock face is not visible by default
        faceSprite.visible = false;

        // clock face is stored as a custom clock property
        clockSprite.face = faceSprite;

        // hand sprite is the clock hand
        let handSprite = this.handGroup.create(clockSprite.x, clockSprite.y, prefix + "hand");
        handSprite.setScale(1.5);

        // set clock hand tint
        handSprite.tint = 0x2babca;

        // set a random clock hand rotation
        handSprite.rotation = Phaser.Math.Angle.Random();

        // set a random angular velocity
        handSprite.body.angularVelocity = Phaser.Math.RND.between(levels[gameOptions.startingLevel].clockSpeed[0], levels[gameOptions.startingLevel].clockSpeed[1]) * Phaser.Math.RND.sign();

        // clock hand is stored as a custom clock property
        clockSprite.hand = handSprite;

        // increase totalCloks
        this.totalClocks ++;

        // return the clock itself
        return clockSprite;
    }

    // method to fire the ball. It's called throwBall rather than fireBall because the game is not a RPG :)
    throwBall() {

        // if we can fire...
        if(this.canFire) {

            // set canFire to false, we are already firing
            this.canFire = false;

            // get active clock hand rotation
            let handAngle = this.activeClock.hand.rotation;

            // place the ball at the same active clock position
            this.ball.x = this.activeClock.x;
            this.ball.y = this.activeClock.y;

            // set the ball visible
            this.ball.visible = true;

            // calculate velocity according to clock angle rotation
            let ballVelocity = this.physics.velocityFromRotation(handAngle, gameOptions.ballSpeed);

            // set ball velocity
            this.ball.body.setVelocity(ballVelocity.x, ballVelocity.y);

            // destroy active clock, clock face and clock hand
            this.activeClock.hand.destroy();
            this.activeClock.face.destroy();
            this.activeClock.destroy();
        }
    }

    // method to handle overlap between ball and clock
    handleOverlap(ball, clock) {

        // can't we fire? (this means: are we firing at the moment)
        if(!this.canFire) {

            // change clock frame and tint color to make it active
            clock.setFrame(1);
            clock.tint = 0x2babca;

            // show clock face
            clock.face.visible = true;

            // change clock hand frame tint and color to make it active
            clock.hand.setFrame(1);
            clock.hand.tint = 0xffffff;

            // now this clock is the active clock
            this.activeClock = clock;

            // hide the ball
            this.ball.visible = false;

            // stop the ball
            this.ball.setVelocity(0, 0);

            // we reached another clock
            this.clocksReached ++;

            // are there more clocks to reach?
            if(this.clocksReached < this.totalClocks) {
                // we can fire again
                this.canFire = true;
                this.updateScore();
            }
            else {
                // advance by one level
                gameOptions.startingLevel = (gameOptions.startingLevel + 1) % levels.length;

                // wait one second and a half, then restart the game
                this.time.addEvent({
                    delay: 1500,
                    callbackScope: this,
                    callback: function() {
                        this.updateScore();
                        gameOptions.score = this.score;
                        this.gameState = PLAY;
                        this.scene.start("GamePlay");
                    }
                });
            }
        }
    }
}

// the levels. 0: empty tile / 1: small clock / 2: big clock

let levels = [
// level 1
{
     clockSpeed: [200, 450],
     tiledOutput: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
// level 2
{
     clockSpeed: [200, 450],
     tiledOutput: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
// level 3
{
     clockSpeed: [200, 450],
     tiledOutput: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
// level 4
{
     clockSpeed: [200, 450],
     tiledOutput: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
// level 5
{
     clockSpeed: [200, 450],
     tiledOutput: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 2, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
// level 6
{
     clockSpeed: [200, 450],
     tiledOutput: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
// level 7
{
     clockSpeed: [200, 450],
     tiledOutput: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
// level 8
{
     clockSpeed: [200, 450],
     tiledOutput: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
},
// level 9
{
     clockSpeed: [200, 450],
     tiledOutput: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0]
},
// level 10
{
     clockSpeed: [200, 450],
     tiledOutput: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 2, 0, 0, 0, 0, 1, 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 2, 0, 0, 0, 0, 0]
}
]

